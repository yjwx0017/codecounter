#ifndef MAINWIDGET_H
#define MAINWIDGET_H

#include <QWidget>
#include "codecounterthread.h"
#include <QHash>
#include <QTextStream>

namespace Ui {
class MainWidget;
}

class MainWidget : public QWidget
{
    Q_OBJECT

public:
    explicit MainWidget(QWidget *parent = 0);
    ~MainWidget();

private slots:
    void on_selectLangTypes_clicked();
    void on_selectSourceDir_clicked();
    void on_start_clicked();
    void on_stop_clicked();
    void on_outputResult_clicked();
    void on_about_clicked();
    void on_quit_clicked();

    void onCountStarted();
    void onCountStopped();
    void onCountFileFinished(
            const FileCountResult& fileResult, const CountResult& result);
    void onCountFinished(const CountResult& result, bool userStop);

    void on_sourceDir_currentIndexChanged(int index);

protected:
    void closeEvent(QCloseEvent* event);

private:
    void initWidgets();
    void loadSourceDirHistory();
    void saveSourceDirHistory();
    void loadLangTypesConfig();
    void saveLangTypesConfig();
    int getTableColumnWidth(int column);
    void outputHeaderLabel(QTextStream& stream, int column, int width);
    void outputTableItem(QTextStream& stream, int row, int column, int width);

private:
    Ui::MainWidget*          ui;
    CodeCounterThread*       mThread;
    QStringList              mLangTypeNames;
    QHash<QString, LangType> mLangTypes;
    QString                  mSourceDir;
};

#endif // MAINWIDGET_H
