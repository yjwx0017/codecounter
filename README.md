使用Qt编写的源代码行数统计小工具。

界面模仿了 [Iris Code Counter](http://www.xiazaiba.com/html/36987.html) 。

![Screenshot](https://gitee.com/yjwx0017/codecounter/raw/master/screenshot.png)

![Screenshot](https://gitee.com/yjwx0017/codecounter/raw/master/screenshot2.png)
