#-------------------------------------------------
#
# Project created by QtCreator 2017-02-17T19:59:30
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = CodeCounter
TEMPLATE = app

CONFIG += c++11

DESTDIR = $$PWD/bin
# mainwindow.cpp
SOURCES += main.cpp \
    mainwidget.cpp \
    codecounterthread.cpp \
    selectlangtypedialog.cpp

HEADERS  +=  \
    mainwidget.h \
    codecounterthread.h \
    selectlangtypedialog.h

RESOURCES +=

TRANSLATIONS = codecounter_zh_CN.rs

FORMS += \
    mainwidget.ui \
    selectlangtypedialog.ui
