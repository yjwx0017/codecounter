#include "selectlangtypedialog.h"
#include "ui_selectlangtypedialog.h"
#include <QListWidgetItem>
#include <QInputDialog>
#include <QMessageBox>

SelectLangTypeDialog::SelectLangTypeDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::SelectLangTypeDialog)
{
    ui->setupUi(this);
}

SelectLangTypeDialog::~SelectLangTypeDialog()
{
    delete ui;
}

void SelectLangTypeDialog::setAllLangTypes(
        const QStringList& names, const QHash<QString, LangType>& langTypes)
{
    mLangTypeNames = names;
    mLangTypes = langTypes;
}
void SelectLangTypeDialog::setSelectedLangTypes(const QStringList& langTypeNames)
{
    mSelectedLangTypes = langTypeNames;
}

void SelectLangTypeDialog::getAllLangTypes(
        QStringList& names, QHash<QString, LangType>& langTypes) const
{
    names = mLangTypeNames;
    langTypes = mLangTypes;
}

QStringList SelectLangTypeDialog::getSelectedLangTypes() const
{
    return mSelectedLangTypes;
}

void SelectLangTypeDialog::showEvent(QShowEvent* event)
{
    foreach (const QString& name, mLangTypeNames)
    {
        QListWidgetItem* item = new QListWidgetItem(name);

        bool contains = mSelectedLangTypes.contains(name);
        item->setCheckState(contains ? Qt::Checked : Qt::Unchecked);

        ui->listWidget->addItem(item);
    }

    QDialog::showEvent(event);
}

void SelectLangTypeDialog::accept()
{
    QStringList selectedNames;

    int rowCount = ui->listWidget->count();
    for (int i=0; i<rowCount; ++i)
    {
        if (ui->listWidget->item(i)->checkState() == Qt::Checked)
            selectedNames.append(ui->listWidget->item(i)->text());
    }

    if (selectedNames.isEmpty())
    {
        QMessageBox msgBox(this);
        msgBox.setWindowTitle("提示");
        msgBox.setText("请至少选择一项！");
        msgBox.exec();
        return;
    }

    mSelectedLangTypes = selectedNames;

    QDialog::accept();
}

void SelectLangTypeDialog::on_addLangType_clicked()
{
    QString name = QInputDialog::getText(this, "添加", "输入名称");
    if (name.isEmpty())
        return;

    if (mLangTypeNames.contains(name))
    {
        QMessageBox msgBox(this);
        msgBox.setWindowTitle("提示");
        msgBox.setText("该名称已存在！");
        msgBox.exec();
        return;
    }

    QListWidgetItem* item = new QListWidgetItem(name);
    item->setCheckState(Qt::Unchecked);
    ui->listWidget->addItem(item);
    ui->listWidget->setCurrentItem(item);

    mLangTypeNames.append(name);
    mLangTypes[name].name = name;
}

void SelectLangTypeDialog::on_removeLangType_clicked()
{
    int row = ui->listWidget->currentRow();
    if (row == -1)
        return;

    QString langTypeName = ui->listWidget->currentItem()->text();
    delete ui->listWidget->takeItem(row);

    mLangTypeNames.removeAll(langTypeName);
    mLangTypes.remove(langTypeName);
}

void SelectLangTypeDialog::on_listWidget_currentItemChanged(
        QListWidgetItem *current, QListWidgetItem * /*previous*/)
{
    ui->name->clear();
    ui->exts->clear();
    ui->singleComment->clear();
    ui->multiCommentStart->clear();
    ui->multiCommentEnd->clear();

    if (current)
    {
        QString name = current->text();
        ui->name->setText(name);

        LangType& langType = mLangTypes[name];
        ui->exts->setText(langType.fileExtensions.join(','));
        ui->singleComment->setText(langType.singleLineComment);
        ui->multiCommentStart->setText(langType.multiLineCommentsBegin);
        ui->multiCommentEnd->setText(langType.multiLineCommentsEnd);
    }
}

void SelectLangTypeDialog::on_update_clicked()
{
    if (ui->name->text().isEmpty())
    {
        QMessageBox msgBox(this);
        msgBox.setWindowTitle("提示");
        msgBox.setText("名称不能为空！");
        msgBox.exec();
        return;
    }

    if (!ui->listWidget->currentItem())
        return;

    QString oldName = ui->listWidget->currentItem()->text();
    QString newName = ui->name->text();
    if (oldName != newName)
    {
        mLangTypeNames[mLangTypeNames.indexOf(oldName)] = newName;
        ui->listWidget->currentItem()->setText(newName);

        mLangTypes.remove(oldName);
        mLangTypes[newName].name = newName;
    }

    LangType& langType = mLangTypes[newName];
    langType.fileExtensions         = ui->exts->text().split(',');
    langType.singleLineComment      = ui->singleComment->text();
    langType.multiLineCommentsBegin = ui->multiCommentStart->text();
    langType.multiLineCommentsEnd   = ui->multiCommentEnd->text();

}
