#include "mainwidget.h"
#include <QApplication>
#include <QDir>

// todo list
// 1. 使用ui文件
// 2. 使用json配置文件，语言：扩展名、单行注释、多行注释（起始结束字符）
// 3. 实现输出结果函数、关于按钮等未实现的功能，关于Qt
// 4. 各种语言，加入配置文件
// 5. 文件类型支持多选，加个按钮选择一个或多个语言
// 6. json配置文件可在界面上配置
// 7. 不再使用翻译文件

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    app.setStyle("fusion");

    QDir::setCurrent(QCoreApplication::applicationDirPath());

    MainWidget w;
    w.show();

    return app.exec();
}
