#ifndef SELECTLANGTYPEDIALOG_H
#define SELECTLANGTYPEDIALOG_H

#include <QDialog>
#include "codecounterthread.h"
#include <QListWidgetItem>

namespace Ui {
class SelectLangTypeDialog;
}

class SelectLangTypeDialog : public QDialog
{
    Q_OBJECT

public:
    explicit SelectLangTypeDialog(QWidget *parent = 0);
    ~SelectLangTypeDialog();

    void setAllLangTypes(
            const QStringList& names, const QHash<QString, LangType>& langTypes);

    void setSelectedLangTypes(const QStringList& langTypeNames);

    void getAllLangTypes(
            QStringList& names, QHash<QString, LangType>& langTypes) const;
    QStringList getSelectedLangTypes() const;

protected:
    void showEvent(QShowEvent* event) override;
    void accept() override;

private slots:
    void on_addLangType_clicked();

    void on_removeLangType_clicked();

    void on_listWidget_currentItemChanged(
            QListWidgetItem *current, QListWidgetItem *previous);

    void on_update_clicked();

private:
    Ui::SelectLangTypeDialog *ui;
    QStringList              mSelectedLangTypes;
    QStringList              mLangTypeNames;
    QHash<QString, LangType> mLangTypes;
};

#endif // SELECTLANGTYPEDIALOG_H
